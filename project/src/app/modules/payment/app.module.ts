import { NgModule } from "@angular/core";

import { MaterialModule } from "../../../app/material-ui.module";
import { AppRoutingModule } from "../../../app/app-routing.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [],
  imports: [MaterialModule, AppRoutingModule, CommonModule, FlexLayoutModule],
  exports: [],
  providers: [],
  bootstrap: []
})
export class AppModulePayment {}
