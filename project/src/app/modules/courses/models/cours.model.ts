export interface Course {
  id?: string;
  key?: string;
  title?: string;
  categorie?: string;
  price?: number;
  urlImage?: string;
  url2Image?: string;
  url3Image?: string;
  url4Image?: string;
  description?: string;
  composition?: string;
  detail?: string;
  collection?: string;
}
