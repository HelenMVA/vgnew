import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { MatGridTileHeaderCssMatStyler } from "@angular/material";
import { map } from "rxjs/operators";
import { Course } from "../models/cours.model";
import { reject } from "q";
@Injectable({
  providedIn: "root"
})
export class CourseService {
  constructor(private db: AngularFireDatabase) {}
  getAllCourses() {
    return this.db
      .list("/courses")
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({
            key: c.payload.key,
            ...c.payload.val()
          }))
        )
      );
  }
  //submit form
  addCourse(course: Course) {
    return this.db.list("/courses/").push({
      categorie: course.categorie,
      description: course.description,
      price: course.price,
      title: course.title,
      urlImage: course.urlImage,
      url2Image: course.url2Image,
      url3Image: course.url3Image,
      url4Image: course.url4Image,
      composition: course.composition,
      detail: course.detail,
      collection: course.collection
    });
  }
  // juste pour categorie update
  getCoursebyId(uid: string) {
    console.log("GETBYID");
    return this.db
      .object("/courses/" + uid)
      .snapshotChanges()
      .pipe(
        map(course => {
          let obj: any = course.payload.val();
          let courseTemp: Course = {
            id: course.key,
            categorie: obj.categorie,
            description: obj.description,
            price: obj.price,
            title: obj.title,
            urlImage: obj.urlImage,
            url2Image: obj.url2Image,
            url3Image: obj.url3Image,
            url4Image: obj.url4Image,
            composition: obj.composition,
            detail: obj.detail,
            collection: obj.collection
          };
          return courseTemp;
        })
      );
  }

  getseuleCourse(id) {
    return new Promise((resolve, reject) => {
      this.db.database
        .ref("/courses/" + id)
        .once("value")
        .then(data => {
          resolve(data.val());
          console.log("service marche");
          console.log(data.val());
        })
        .catch(error => reject(error));
    });
  }

  updateCourse(course: Course) {
    return this.db.object("/courses/" + course.id).update({
      title: course.title,
      // description: course.description,
      categorie: course.categorie,
      description: course.description,
      price: course.price,
      urlImage: course.urlImage,
      url2Image: course.url2Image,
      url3Image: course.url3Image,
      url4Image: course.url4Image,
      composition: course.composition,
      detail: course.detail,
      collection: course.collection
    });
  }
  deleteCourse(id: string) {
    return this.db.object("/courses/" + id).remove();
  }
}
