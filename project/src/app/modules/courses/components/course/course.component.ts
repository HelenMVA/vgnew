import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CategorieService } from "src/app/modules/commun/services/categorie.service";
import { CourseService } from "../../services/course.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Course } from "../../models/cours.model";
import { map, mergeMap } from "rxjs/operators";

@Component({
  selector: "app-course",
  templateUrl: "./course.component.html",
  styleUrls: ["./course.component.css"]
})
export class CourseComponent implements OnInit {
  course: Course;
  regiForm: FormGroup;
  categories: any[];
  constructor(
    private fb: FormBuilder,
    private serviceCategirie: CategorieService,
    private serviceCourse: CourseService,
    public dialogRef: MatDialogRef<CourseComponent>,
    @Inject(MAT_DIALOG_DATA) public idCourse
  ) {
    this.regiForm = this.fb.group({
      title: [null, Validators.required],
      price: [null, Validators.required],
      urlImage: [null, Validators.required],
      url2Image: [null, Validators.required],
      url3Image: [null, Validators.required],
      url4Image: [null, Validators.required],
      description: [null, Validators.required],
      composition: [null, Validators.required],
      detail: [null, Validators.required],
      collection: [null, Validators.required],
      categorie: [null, Validators.required]
    });
  }

  ngOnInit() {
    //select categories
    if (!this.idCourse) {
      console.log("test1");
      this.serviceCategirie
        .getAllCategories()
        .subscribe(categories => (this.categories = categories));
    } else {
      this.serviceCategirie
        .getAllCategories()
        .pipe(
          mergeMap(categories =>
            this.serviceCourse.getCoursebyId(this.idCourse.id).pipe(
              map(course => {
                console.log("test2");
                return [categories, course];
              })
            )
          )
        )
        .subscribe(([categories, course]) => {
          console.log("test3");
          this.categories = categories as any[];
          this.course = course as Course;
          this.initalizeCourse(course);
        });
    }
  }
  initalizeCourse(course) {
    console.log("test4????");
    this.regiForm = this.fb.group({
      title: [course ? course.title : null, Validators.required],
      price: [course ? course.price : null, Validators.required],
      urlImage: [course ? course.urlImage : null, Validators.required],
      url2Image: [course ? course.url2Image : null, Validators.required],
      url3Image: [course ? course.url3Image : null, Validators.required],
      url4Image: [course ? course.url4Image : null, Validators.required],
      description: [course ? course.description : null, Validators.required],
      composition: [course ? course.composition : null, Validators.required],
      detail: [course ? course.detail : null, Validators.required],
      collection: [course ? course.collection : null, Validators.required],
      categorie: [course ? course.categorie : null, Validators.required]
    });
  }

  //submit form
  onSubmit(form) {
    console.log(form);
    if (this.regiForm.valid) {
      let course: Course = {
        // key: "",
        id: this.idCourse ? this.idCourse.id : "",
        title: form.title,
        price: form.price,
        urlImage: form.urlImage,
        url2Image: form.url2Image,
        url3Image: form.url3Image,
        url4Image: form.url4Image,
        description: form.description,
        composition: form.composition,
        detail: form.detail,
        collection: form.collection,
        categorie: form.categorie
      };
      if (!this.idCourse) {
        console.log("!this.idCourse");
        this.serviceCourse.addCourse(course).then(() => {
          this.dialogRef.close();
        });
      } else {
        console.log("else");
        this.serviceCourse.updateCourse(course).then(() => {
          this.dialogRef.close();
        });
      }
    }
  }
}
