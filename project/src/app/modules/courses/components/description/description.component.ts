import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { CourseService } from "../../services/course.service";
import { Course } from "../../models/cours.model";
import { trigger, transition, style, animate } from "@angular/animations";
import {ShoppingCartService} from "../../../shoppingCard/services/shopping-cart.service"

@Component({
  selector: "app-description",
  templateUrl: "./description.component.html",
  styleUrls: ["./description.component.css"],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({ height: 400, opacity: 0 }),
        animate("0.4s ease-out", style({ height: 700, opacity: 1 }))
      ])
    ]),
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 }))
      ])
    ])
  ]
})
export class DescriptionComponent implements OnInit {
  courses: Course = {
    title: "Not Loaded",
    categorie: "",
    price: 0,
    key: "",
    urlImage: "",
    url2Image: "",
    url3Image: "",
    url4Image: "",
    description: "There is a problem, please contact our support "
  };
  constructor(
    private router: ActivatedRoute,
    private courseService: CourseService,
    private serviceShoppingCart: ShoppingCartService
  ) {}

  ngOnInit() {
    window.scrollTo(0, 0);
    const id = this.router.snapshot.paramMap.get("id");
    this.courseService
      .getseuleCourse(id)
      .then((courses: Course) => {
        this.courses = courses;
        console.log("tout est bien");
        console.log(courses);
      })
      .catch(error => {
        console.error(error);
      });
  }

  isPrinsipal = true;
  isDetail = false;
  check = false;
  check2 = false;
  check3 = false;
  check4 = false;
  isShow = true;
  isShow2 = false;
  isShow3 = false;
  isShow4 = false;
  goPhoto2() {
    this.isShow = false;
    this.isShow2 = true;
    this.isShow3 = false;
    this.isShow4 = false;
    this.check = false;
    this.check2 = true;
    this.check3 = false;
    this.check4 = false;
  }
  goPhoto() {
    this.isShow = true;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = false;
    this.check = true;
    this.check2 = false;
    this.check3 = false;
    this.check4 = false;
  }
  goPhoto3() {
    this.isShow = false;
    this.isShow2 = false;
    this.isShow3 = true;
    this.isShow4 = false;
    this.check = false;
    this.check2 = false;
    this.check3 = true;
    this.check4 = false;
  }
  goPhoto4() {
    this.isShow = false;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = true;
    this.check = false;
    this.check2 = false;
    this.check3 = false;
    this.check4 = true;
  }
  goDetails() {
    this.isPrinsipal = false;
    this.isDetail = true;
  }
  goClose() {
    this.isPrinsipal = true;
    this.isDetail = false;
  }

  // add cart =>pagnie
  AddtoCart(courses) {
    console.log(courses, "ts course");
    this.serviceShoppingCart.AddtoCart(courses);
  }
  // fin add cart =>pagnie
}
