import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscriber, Subscription, from } from "rxjs";
import { CategorieService } from "src/app/modules/commun/services/categorie.service";
import { CourseService } from "../../services/course.service";
import { mergeMap, map } from "rxjs/operators";
import { ShoppingCartService } from "src/app/modules/shoppingCard/services/shopping-cart.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-courses",
  templateUrl: "./courses.component.html",
  styleUrls: ["./courses.component.css"]
})
export class CoursesComponent implements OnInit, OnDestroy {
  check = false;
  check2 = false;
  categories: any[];
  courses: any[];
  sub: Subscription;

  constructor(
    private serviceCategorie: CategorieService,
    private serviceCourses: CourseService,
    private serviceShoppingCart: ShoppingCartService,
    private router: Router
  ) {}
  ngOnInit() {
    window.scrollTo(0, 0);
    this.sub = this.serviceCategorie
      .getAllCategories()
      .pipe(
        mergeMap(categories =>
          this.serviceCourses
            .getAllCourses()
            .pipe(map(courses => [categories, courses]))
        )
      )
      .subscribe(([categories, courses]) => {
        this.categories = categories;
        this.courses = courses;
        // console.log(courses);
        // console.log(categories);
      });
  }
  getCoursesByCategorie(key) {
    return this.courses.filter(course => course.categorie == key);
  }

  AccessCourse(key) {
    this.router.navigate(["/description", key]);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
  // add cart =>pagnie
  // AddtoCart(course) {
  //   this.serviceShoppingCart.addtoCart(course);
  // } deplacé dans description
  //FAVORITE
  Addtofavorites(course) {
    this.check = true;
    this.check2 = true;
    this.serviceShoppingCart.addtofavorites(course);
  }
}
