import { NgModule } from "@angular/core";
import { CoursesComponent } from "./components/courses/courses.component";
import { MaterialModule } from "src/app/material-ui.module";
import { AppModuleMenu } from "../menu/app.module";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CourseComponent } from "./components/course/course.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DescriptionComponent } from "./components/description/description.component";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  declarations: [CoursesComponent, CourseComponent, DescriptionComponent],
  imports: [
    MaterialModule,
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BrowserAnimationsModule,
    BrowserModule
  ],
  exports: [CourseComponent, DescriptionComponent],
  providers: [],
  bootstrap: []
})
export class AppModuleCourses {}
