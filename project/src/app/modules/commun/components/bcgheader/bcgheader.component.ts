import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
  selector: "app-bcgheader",
  templateUrl: "./bcgheader.component.html",
  styleUrls: ["./bcgheader.component.css"]
})
export class BcgheaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $("#test").on("click", function() {
      /**
       * Smooth scrolling to a specific element
       **/
      function goSectionII(target: JQuery<HTMLElement>) {
        if (target.length) {
          $("html, body")
            .stop()
            .animate({ scrollTop: target.offset().top }, 900);
        }
      }
      // exemple
      goSectionII($("#goici"));
    });
  }
}
