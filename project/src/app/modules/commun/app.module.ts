import { NgModule } from "@angular/core";
import { HomeComponent } from "./components/home/home.component";
import { AboutComponent } from "./components/about/about.component";
import { MainComponent } from "./components/main/main.component";
import { BcgheaderComponent } from "./components/bcgheader/bcgheader.component";
import { Section1Component } from "./components/section1/section1.component";
import { Section2Component } from "./components/section2/section2.component";
import { MaterialModule } from "src/app/material-ui.module";
import { AppRoutingModule } from "src/app/app-routing.module";

@NgModule({
  declarations: [
    HomeComponent,
    AboutComponent,
    MainComponent,
    BcgheaderComponent,
    Section1Component,
    Section2Component
  ],
  imports: [MaterialModule, AppRoutingModule],
  exports: [
    Section1Component,
    Section2Component,
    BcgheaderComponent,
    HomeComponent
  ],
  providers: [],
  bootstrap: []
})
export class AppModuleCommun {}
