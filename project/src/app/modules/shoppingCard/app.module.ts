import { NgModule } from "@angular/core";
import { MaterialModule } from "src/app/material-ui.module";
import { AppRoutingModule } from "src/app/app-routing.module";
import { PanierComponent } from "./components/panier/panier.component";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [PanierComponent],
  imports: [MaterialModule, AppRoutingModule, CommonModule],
  exports: [PanierComponent],
  providers: [],
  bootstrap: []
})
export class AppModuleShoppingCarg {}
