import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { take, map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ShoppingCartService {
  constructor(private db: AngularFireDatabase) {}

  // add cart =>pagnie// deplacé dans descrition
  // async addtoCart(course) {
  //   console.log("1");
  //   let cartId = localStorage.getItem("cartId");

  //   if (!cartId) {
  //     console.log("2");
  //     let cart = await this.db.list("/shoppingCart").push({
  //       dataCreated: new Date().getTime()
  //     });
  //     console.log("3");
  //     localStorage.setItem("cartId", cart.key);
  //     console.log(cart.key);
  //     this.addCourseCart(cart.key, course);
  //     console.log(course, "course AddToCart() set key");
  //   } else {
  //     console.log("4");
  //     this.addCourseCart(localStorage.getItem("cartId"), course);
  //   }
  // }

  async AddtoCart(courses) {
    // description
    console.log("1");
    // console.log(courses, "bang");
    let cartId = localStorage.getItem("cartId");

    if (!cartId) {
      console.log("2");
      let cart = await this.db.list("/shoppingCart").push({});
      // let cart = await this.db.list("/shoppingCart").push({
      //   dataCreated: new Date().getTime()
      // });
      console.log("3");
      localStorage.setItem("cartId", cart.key);
      // console.log(cart.key);
      // console.log("J'envoie ça :");
      // console.log(cart.key, courses);
      this.AddCourseCart(cart.key, courses);
      console.log(courses, "courses AddToCart() set key");
    } else {
      // console.log("4");
      // console.log("J'envoie ça :");
      // console.log(localStorage.getItem("cartId"), courses);
      this.AddCourseCart(localStorage.getItem("cartId"), courses);
    }
  }

  AddCourseCart(idCart, course) {
    // description
    console.log("5");
    console.log(course, "course");
    console.log(idCart);
    var lol = localStorage.getItem("cartId");
    lol += 1;
    localStorage.setItem("cartId", lol);
    this.db
      .object("/shoppingCart/" + lol + "/items/" + course.key)
      .snapshotChanges()
      .pipe(take(1))
      .subscribe(courseCart => {
        console.log("6");
        console.log(course.key, "lol");
        if (!courseCart.key) {
          console.log("7");
          console.log(course, "coursecart");
          console.log(courseCart.key);

          course.key = localStorage.getItem("cartId");
          this.db
            .list("/shoppingCart/")
            // .list("/shoppingCart/" + idCart + "/items/")
            .set(localStorage.getItem("cartId"), { courses: course });

          // console.log(courseCart.key);
          // console.log("courseAdd.key");
          // console.log(course);
        }
      });
  }
  // deplacé description
  // addCourseCart(idCart, courseAdd) {
  //   console.log("5");
  //   this.db
  //     .object("/shoppingCart/" + idCart + "/items/" + courseAdd.key)
  //     .snapshotChanges()
  //     .pipe(take(1))
  //     .subscribe(courseCart => {
  //       console.log("6");
  //       if (!courseCart.key) {
  //         console.log("7");
  //         console.log(courseCart, "coursecart");
  //         this.db
  //           .list("/shoppingCart/" + idCart + "/items/")
  //           .set(courseAdd.key, { course: courseAdd });

  //         console.log(courseCart.key);
  //         console.log(courseAdd.key, "courseAdd.key");
  //         console.log(courseAdd);
  //       }
  //     });
  // }

  // fin add cart =>pagnie

  //FAVORITES
  async addtofavorites(course) {
    let cartId = localStorage.getItem("cartId");
    if (!cartId) {
      let cart = await this.db.list("/favoritesCart").push({
        dataCreated: new Date().getTime()
      });
      localStorage.setItem("cartId", cart.key);
      this.addFavoritesCart(cart.key, course);
    } else {
      this.addFavoritesCart(localStorage.getItem("cartId"), course);
    }
  }

  addFavoritesCart(idCart, courseAdd) {
    this.db
      .object("/favoritesCart/" + courseAdd.Key)
      .snapshotChanges()
      .pipe(take(1))
      .subscribe(courseCart => {
        if (!courseCart.key) {
          this.db
            .list("/favoritesCart/")
            // .list("/favoritesCart/" + idCart + "/items/")
            .set(courseAdd.key, { course: courseAdd });
        }
      });
  }

  // FIN  FAVORITES

  getListItemsShoppingCart() {
    let cartId = localStorage.getItem("cartId");
    return this.db
      .list("/shoppingCart/")
      .snapshotChanges()
      .pipe(
        map(courses =>
          courses.map(c => ({
            key: c.payload.key,
            ...c.payload.val()
          }))
        )
      );
  }
  //panier
  getListItemsShoppingCartPanier() {
    console.log("???");
    return this.db.list("shoppingCart").valueChanges();

    //   let cartId = localStorage.getItem("cartId");
    //   return this.db
    //     .list("shoppingCart")
    //     .snapshotChanges()
    //     .pipe(
    //       map(courses =>
    //         courses.map(c => ({
    //           key: c.payload.key,
    //           ...(c.payload.val() as any).course
    //         }))
    //       )
    //     );
  }

  deleteCourseShoppingCart(id: string) {
    let cartId = localStorage.getItem("cartId");
    return this.db.object("/shoppingCart/" + id).remove();
  }

  clearShpoppingCart() {
    let cartId = localStorage.getItem("cartId");
    // this.db.object("/shoppingCart/" + cartId + "/items/").remove();
    this.db.object("/shoppingCart/").remove();
  }
}
