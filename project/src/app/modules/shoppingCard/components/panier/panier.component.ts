import { Component, OnInit } from "@angular/core";
import { ShoppingCartService } from "../../services/shopping-cart.service";
import { MatTableDataSource } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "app-panier",
  templateUrl: "./panier.component.html",
  styleUrls: ["./panier.component.css"]
})
export class PanierComponent implements OnInit {
  coursesShopping: any[];
  displayedColumns: string[] = ["urlImage", "title", "Price", "actions"];

  constructor(
    private shoppingCart: ShoppingCartService,
    private router: Router
  ) {}

  ngOnInit() {
    this.shoppingCart.getListItemsShoppingCartPanier().subscribe(courses => {
      this.coursesShopping = courses;
      console.log(this.coursesShopping);
    });
  }
  getTotal() {
    let total: number = 0;
    if (!this.coursesShopping) return total;
    this.coursesShopping.forEach(course => {
      total = total + course.courses.price;
    });
    return total;
  }
  //????????????? chemin???????
  Delete(row) {
    console.log(row);
    this.shoppingCart.deleteCourseShoppingCart(row);
  }
  onNext() {
    this.router.navigate(["/orders"]);
  }
}
