import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class RegstrService {
  private eventAutherror = new BehaviorSubject<string>("");
  eventAutherror$ = this.eventAutherror.asObservable();
  newUser: any;
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router
  ) {}

  createUserServ(user) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(userCredential => {
        this.newUser = user;
        console.log(userCredential);
        userCredential.user.updateProfile({
          displayName: user.firstName
        });
        this.inserUserData(userCredential).then(() => {
          console.log("AAAAAA");
          this.router.navigate(["/"]);
        });
      })

      .catch(error => {
        this.eventAutherror.next(error);
        this.router.navigate(["/login"]);
      });
  }
  inserUserData(userCredential: firebase.auth.UserCredential) {
    console.log("?????????");
    return this.db.doc(`Users/${userCredential.user.uid}`).set({
      email: this.newUser.email,
      firstName: this.newUser.firstName,
      lastName: this.newUser.lastName,
      phone: this.newUser.phone
    });
  }
  ///////////
  getUserState() {
    return this.afAuth.authState;
  }
  login(email: string, password: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .catch(error => {
        this.router.navigate(["/register"]);
        this.eventAutherror.next(error);
      })
      .then(userCredential => {
        if (userCredential) {
          this.router.navigate(["/"]);
        }
      });
  }

  /////////////
}
