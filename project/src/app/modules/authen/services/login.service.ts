import { Injectable, NgZone } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import * as firebase from "firebase";
import { CanActivate, Router } from "@angular/router";
import { map, switchMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { UserService } from "../../users/services/user.service";
import { async } from "q";

@Injectable({
  providedIn: "root"
})
export class LoginService implements CanActivate {
  constructor(
    private ngZone: NgZone,
    private login: AngularFireAuth,
    private router: Router,
    private serviceUser: UserService
  ) {
    this.login.authState.subscribe(user => this.serviceUser.saveUser(user)); //save user in db
  }
  loginWithGoogle() {
    this.login.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }
  // async loginWithGoogle() {
  //   this.login.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  //   await this.ngZone.run(() => {
  //     this.router.navigate(["/courses"]);
  //   });
  // }
  loginwithFacebook() {
    this.login.auth.signInWithRedirect(
      new firebase.auth.FacebookAuthProvider()
    );
  }
  logoutWithGoogle() {
    return this.login.auth.signOut();
  }
  getCurrentUser() {
    return this.login.authState;
  }
  //protecting the routes
  canActivate(): Observable<boolean> {
    return this.login.authState.pipe(
      map(user => {
        if (user) return true;
        else {
          this.router.navigate(["/login"]);
          return false;
        }
      })
    );
  }

  // Reset Forggot password
  async ForgotPassword(passwordResetEmail) {
    try {
      await this.login.auth.sendPasswordResetEmail(passwordResetEmail);
      window.alert("Nouveau mot de passe envoyter sur votre mail");
    } catch (error) {
      window.alert(error);
    }
  }
  // Reset Forggot password

  //protect admon root
  getCurrentUserDb() {
    return this.login.authState.pipe(
      switchMap(user => {
        try {
          return this.serviceUser.getUserByuid(user.uid);
        } catch (error) {
          console.log(error);
        }
      }),
      map(user => {
        return user;
      })
    );
  }
}
