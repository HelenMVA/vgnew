import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { LoginComponent } from "./components/login/login.component";
import { MaterialModule } from "src/app/material-ui.module";
import { LeftloginComponent } from "./components/leftlogin/leftlogin.component";
import { RightregistrComponent } from "./components/rightregistr/rightregistr.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; // <== add the imports!
import { AngularFirestore } from '@angular/fire/firestore';

@NgModule({
  declarations: [
    LoginComponent,
    LeftloginComponent,
    RightregistrComponent,
    LeftloginComponent,
    RightregistrComponent
  ],
  imports: [MaterialModule, BrowserModule, FormsModule, ReactiveFormsModule],
  exports: [LeftloginComponent, RightregistrComponent, LoginComponent],
  providers: [AngularFirestore],
  bootstrap: []
})
export class AppModuleAuth {}
