import { Component, OnInit } from "@angular/core";
import { LoginService } from "../../services/login.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  constructor(private login: LoginService) {}
  ngOnInit() {}
  isShow = true;
  isShow2 = false;

  toggleDisplay() {
    // this.isShow = !this.isShow;
    this.isShow = true;
    this.isShow2 = false;
  }
  toggleDisplay2() {
    // this.isShow = !this.isShow;
    this.isShow2 = true;
    this.isShow = false;
  }

  onLogin() {
    this.login.loginWithGoogle();
  }
  onLoginFac() {
    this.login.loginwithFacebook();
  }
  // onLogout() {
  //   this.login.logoutWithGoogle();
  // }
}
