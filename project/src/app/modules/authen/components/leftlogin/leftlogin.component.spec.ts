import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftloginComponent } from './leftlogin.component';

describe('LeftloginComponent', () => {
  let component: LeftloginComponent;
  let fixture: ComponentFixture<LeftloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
