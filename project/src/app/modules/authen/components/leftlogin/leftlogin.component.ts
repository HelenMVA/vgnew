import { Component, OnInit } from "@angular/core";
import { RegstrService } from "../../services/regstr.service";
import { LoginService } from "../../services/login.service";

@Component({
  selector: "app-leftlogin",
  templateUrl: "./leftlogin.component.html",
  styleUrls: ["./leftlogin.component.css"]
})
export class LeftloginComponent implements OnInit {
  constructor(
    private auth: RegstrService,
    private loginService: LoginService
  ) {}

  ngOnInit() {}

  cache = false;
  montre() {
    this.cache = true;
  }
  login(frm) {
    this.auth.login(frm.value.email, frm.value.password);
  }
  // Reset Forggot password
  mpOublie(passwordResetEmail) {
    this.loginService.ForgotPassword(passwordResetEmail);
    console.log(passwordResetEmail);
  }
  // Reset Forggot password
}
