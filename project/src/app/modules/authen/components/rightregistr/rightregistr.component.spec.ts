import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightregistrComponent } from './rightregistr.component';

describe('RightregistrComponent', () => {
  let component: RightregistrComponent;
  let fixture: ComponentFixture<RightregistrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightregistrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightregistrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
