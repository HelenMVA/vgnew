import { Component, OnInit } from "@angular/core";
import { LoginService } from "../../services/login.service";
import { Router } from "@angular/router";
import { RegstrService } from "../../services/regstr.service";

@Component({
  selector: "app-rightregistr",
  templateUrl: "./rightregistr.component.html",
  styleUrls: ["./rightregistr.component.css"]
})
export class RightregistrComponent implements OnInit {
  authError: any;
  constructor(private router: Router, private auth: RegstrService) {}

  ngOnInit() {
    this.auth.eventAutherror$.subscribe(data => {
      this.authError = data;
    });
  }

  createUser(frm) {
    this.auth.createUserServ(frm.value);
    this.router.navigate(["/courses"]);
  }
}
