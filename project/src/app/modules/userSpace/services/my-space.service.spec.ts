import { TestBed } from '@angular/core/testing';

import { MySpaceService } from './my-space.service';

describe('MySpaceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MySpaceService = TestBed.get(MySpaceService);
    expect(service).toBeTruthy();
  });
});
