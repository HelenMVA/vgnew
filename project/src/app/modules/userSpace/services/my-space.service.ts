import { Injectable } from "@angular/core";
import { Favorite } from "../models/favorite.model";
import { AngularFireDatabase } from "@angular/fire/database";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class MySpaceService {
  adressAPI = "http://localhost:3000/send-email";
  favoritesCollection: AngularFirestoreCollection<Favorite>;
  favorites: Observable<Favorite[]>;
  constructor(private db: AngularFireDatabase, private httpClient: HttpClient) {
    // this.favorites = this.db.list("favoritesCart").valueChanges();
  }
  sendContact(body) {
    console.log("service");
    return this.httpClient.post(this.adressAPI, body);
  }

  getFavorite() {
    return this.db.list("favoritesCart").valueChanges();
  }
}
