export interface Favorite {
  id?: string;
  title?: string;
  categorie?: string;
  price?: number;
  urlImage?: string;
  url2Image?: string;
  url3Image?: string;
  url4Image?: string;
  description?: string;
}
