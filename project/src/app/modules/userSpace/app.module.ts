import { NgModule } from "@angular/core";
import { AppRoutingModule } from "../../app-routing.module";
import { MaterialModule } from "../../../app/material-ui.module";
import { MySpaceComponent } from "./components/my-space/my-space.component";
import { AccountDetailsComponent } from "./components/account-details/account-details.component";
import { FavoritesComponent } from "./components/favorites/favorites.component";
import { OrderHistoryComponent } from "./components/order-history/order-history.component";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { ContactformComponent } from "./components/contactform/contactform.component";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    MySpaceComponent,
    AccountDetailsComponent,
    FavoritesComponent,
    OrderHistoryComponent,
    ContactformComponent
  ],
  imports: [
    MaterialModule,
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    MySpaceComponent,
    OrderHistoryComponent,
    FavoritesComponent,
    AccountDetailsComponent
  ],
  providers: [],
  bootstrap: []
})
export class AppModuleUserSpace {}
