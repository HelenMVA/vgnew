import { Component, OnInit } from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-order-history",
  templateUrl: "./order-history.component.html",
  styleUrls: ["./order-history.component.css"],
  animations: [
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 1 }),
        animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
      ])
    ])
  ]
})
export class OrderHistoryComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
