import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/modules/authen/services/login.service";
import { MySpaceService } from "../../services/my-space.service";
import { Favorite } from "../../models/favorite.model";

@Component({
  selector: "app-my-space",
  templateUrl: "./my-space.component.html",
  styleUrls: ["./my-space.component.css"]
})
export class MySpaceComponent implements OnInit {
  // favorite: Favorite[];
  user: firebase.User;
  constructor(
    private router: Router,
    private auth: LoginService,
    private favoritService: MySpaceService,
    private login: LoginService
  ) {}

  ngOnInit() {
    this.auth.getCurrentUser().subscribe(user => {
      this.user = user;
    });
    // this.favoritService.getFavorite().subscribe(favorite => {
    //   console.log(favorite);
    //   this.favorite = favorite;
    // });
  }
  isShow1 = false;
  isShow2 = true;
  isShow3 = false;
  isShow4 = false;
  myDisplay1() {
    this.isShow1 = true;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = false;
  }
  myDisplay2() {
    this.isShow1 = false;
    this.isShow2 = true;
    this.isShow3 = false;
    this.isShow4 = false;
  }
  myDisplay3() {
    this.isShow1 = false;
    this.isShow2 = false;
    this.isShow3 = true;
    this.isShow4 = false;
  }
  myDisplay4() {
    this.isShow1 = false;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = true;
  }
  logout() {
    this.login.logoutWithGoogle();
    // this.router.navigate(["/"]);
    window.location.reload();
  }
}
