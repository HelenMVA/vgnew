import { Component, OnInit } from "@angular/core";
import { Favorite } from "../../models/favorite.model";
import { MySpaceService } from "../../services/my-space.service";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-favorites",
  templateUrl: "./favorites.component.html",
  styleUrls: ["./favorites.component.css"],
  animations: [
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 1 }),
        // animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
        animate("350ms")
      ])
    ])
  ]
})
export class FavoritesComponent implements OnInit {
  favorite: Favorite[];
  constructor(private favoritService: MySpaceService) {}

  ngOnInit() {
    this.favoritService.getFavorite().subscribe(favorite => {
      console.log(favorite);
      this.favorite = favorite;
    });
    console.log("ngOnInit run");
  }
}
