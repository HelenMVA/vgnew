import { Component, OnInit } from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-account-details",
  templateUrl: "./account-details.component.html",
  styleUrls: ["./account-details.component.css"],
  animations: [
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 1 }),
        animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
      ])
    ])
  ]
})
export class AccountDetailsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
