import { Component, OnInit } from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";
import { HttpClient } from "@angular/common/http";
import { MySpaceService } from "../../services/my-space.service";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-contactform",
  templateUrl: "./contactform.component.html",
  styleUrls: ["./contactform.component.css"],
  animations: [
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 1 }),
        // animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
        animate("350ms")
      ])
    ])
  ]
})
export class ContactformComponent implements OnInit {
  profileForm = new FormGroup({
    name: new FormControl(""),
    phone: new FormControl(""),
    // category: new FormControl(""),
    message: new FormControl("")
  });
  constructor(
    private httpClient: HttpClient,
    private contactService: MySpaceService
  ) {}

  onSubmit() {
    console.log(this.profileForm.value);
    this.contactService.sendContact(this.profileForm.value).subscribe(() => {
      console.log("ca marche");
    });
  }
  ngOnInit() {}
}
