import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { AngularFireAuth } from "@angular/fire/auth";
import { CanActivate, Router } from "@angular/router";
import { LoginService } from "../../authen/services/login.service";
import { switchMap, map } from "rxjs/operators";
import { UserService } from "../../users/services/user.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AdminService {
  constructor(
    private db: AngularFireDatabase,
    private login: LoginService,
    private usersSvc: UserService,
    private route: Router
  ) {}
  //protect admon root
  canActivate(): Observable<boolean> {
    return this.login.getCurrentUserDb().pipe(
      map(user => {
        if (!user) return false;
        if (user.isAdmin) return true;
        this.route.navigate["/login"];
        return false;
      })
    );
  }
}
