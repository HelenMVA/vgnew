import { Component, OnInit } from "@angular/core";
import { MatTableDataSource, MatDialog } from "@angular/material";
import { CourseService } from "src/app/modules/courses/services/course.service";
import { CourseComponent } from "src/app/modules/courses/components/course/course.component";
export interface PeriodicElement {}

@Component({
  selector: "app-admin-courses",
  templateUrl: "./admin-courses.component.html",
  styleUrls: ["./admin-courses.component.css"]
})
export class AdminCoursesComponent implements OnInit {
  courses: any[];
  displayedColumns: string[] = [
    "title",
    "categorie",
    "description",
    "urlImage",
    "url2Image",
    "url3Image",
    "url4Image",
    "price",
    "composition",
    "detail",
    "collection",
    "actions"
  ];

  applyFilter(filterValue: string) {}
  constructor(
    private serviceCourses: CourseService,
    private serviceDialog: MatDialog
  ) {}

  ngOnInit() {
    this.serviceCourses
      .getAllCourses()
      .subscribe(courses => (this.courses = courses));
  }
  addCourse() {
    this.serviceDialog.open(CourseComponent, {
      width: "650px"
    });
  }
  Edit(row) {
    console.log(row.key);
    this.serviceDialog.open(CourseComponent, {
      width: "650px",
      data: { id: row.key }
    });
  }
  Delete(row) {
    if (window.confirm("Are you sure??"))
      this.serviceCourses.deleteCourse(row.key);
  }
}
