import { Component, OnInit } from "@angular/core";
import { ShoppingCartService } from "../../../shoppingCard/services/shopping-cart.service";
import { Router } from "@angular/router";
import { LoginService } from "../../../authen/services/login.service";
import { OrderService } from "../../../orders/services/order.service";
import { PaymentService } from "../../../payment/services/payment.service";

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"]
})
export class OrdersComponent implements OnInit {
  coursesOrder: any[];
  displayedColumns: string[] = ["title", "Price"];
  user;
  constructor(
    private shoppingCart: ShoppingCartService,
    private router: Router,
    private loginService: LoginService,
    private orderService: OrderService,
    private paymentService: PaymentService
  ) {}

  ngOnInit() {
    this.shoppingCart.getListItemsShoppingCartPanier().subscribe(courses => {
      this.coursesOrder = courses;
      console.log(this.coursesOrder);
    });
    this.loginService.getCurrentUserDb().subscribe(user => (this.user = user));
  }
  getTotal() {
    let total: number = 0;
    if (!this.coursesOrder) return total;
    this.coursesOrder.forEach(course => {
      total = total + course.courses.price;
      console.log(total);
    });
    return total;
  }
  onCancel() {
    this.router.navigate(["/courses"]);
  }
  async onPay() {
    //creer order
    let order = {
      dateCreated: new Date().getTime(),
      userId: this.user.id,
      item: this.coursesOrder,
      total: this.getTotal(),
      paid: true
      // paid: faulse   avec vrais payment
    };
    let orderResult: any = await this.orderService.createOrder(order);
    this.shoppingCart.clearShpoppingCart();
    this.router.navigate(["/success-orde", orderResult.key]);

    //clear the chopping cart
    // let resultPayment = this.paymentService.payment(
    //   orderResult.key,
    //   this.getTotal()
    // );
    // if(resultPayment){

    // } else {

    // }
  }
}
