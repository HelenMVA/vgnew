import { NgModule } from "@angular/core";

import { MaterialModule } from "../../material-ui.module";
import { AppRoutingModule } from "../../app-routing.module";

import { OrdersComponent } from "./components/orders/orders.component";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { SuccessOrderComponent } from './components/success-order/success-order.component';

@NgModule({
  declarations: [OrdersComponent, SuccessOrderComponent],
  imports: [MaterialModule, AppRoutingModule, CommonModule, FlexLayoutModule],
  exports: [OrdersComponent],
  providers: [],
  bootstrap: []
})
export class AppModuleOrders {}
