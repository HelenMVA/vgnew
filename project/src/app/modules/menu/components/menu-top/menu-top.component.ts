import { Component, OnInit } from "@angular/core";
import { LoginService } from "src/app/modules/authen/services/login.service";
import { switchMap, map, mergeMap } from "rxjs/operators";
import { Router } from "@angular/router";
import { ShoppingCartService } from "src/app/modules/shoppingCard/services/shopping-cart.service";

@Component({
  selector: "app-menu-top",
  templateUrl: "./menu-top.component.html",
  styleUrls: ["./menu-top.component.css"]
})
export class MenuTopComponent implements OnInit {
  nbrShoppingCourse: number = 0; // pour pagnie
  user: firebase.User;
  constructor(
    private login: LoginService,
    private router: Router,
    private shoppingCart: ShoppingCartService
  ) {}

  ngOnInit() {
    this.login
      .getCurrentUser()
      .pipe(
        switchMap(user => {
          return this.login.getCurrentUserDb();
        }),
        mergeMap(userDb =>
          this.shoppingCart.getListItemsShoppingCart().pipe(
            map(coursesShopping => {
              return [userDb, coursesShopping];
            })
          )
        ),

        map(user => user)
      )
      .subscribe(
        ([userDb, coursesShopping]) => {
          if (userDb != "e") {
            this.nbrShoppingCourse = (coursesShopping as any[]).length;
            this.user = userDb;
          } else this.user = null;
        },
        erreur => console.log
      );
    // показыывть кнопку админскую только админу
  }
  logout() {
    this.login.logoutWithGoogle();
    // this.router.navigate(["/"]);
    window.location.reload();
  }
  goPanier() {
    if (this.nbrShoppingCourse <= 0) return;
    this.router.navigate(["/cart"]);
  }
}
