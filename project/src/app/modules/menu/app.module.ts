import { NgModule } from "@angular/core";
import { MenuTopComponent } from "./components/menu-top/menu-top.component";
import { MaterialModule } from "src/app/material-ui.module";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FooterComponent } from "./components/footer/footer.component";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  declarations: [MenuTopComponent, FooterComponent],
  imports: [MaterialModule, AppRoutingModule, CommonModule],
  exports: [MenuTopComponent, FooterComponent],
  providers: [],
  bootstrap: []
})
export class AppModuleMenu {}
