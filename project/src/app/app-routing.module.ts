import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./modules/commun/components/home/home.component";
import { CoursesComponent } from "./modules/courses/components/courses/courses.component";
import { LoginComponent } from "./modules/authen/components/login/login.component";
import { AboutComponent } from "./modules/commun/components/about/about.component";
import { OrdersComponent } from "./modules/orders/components/orders/orders.component";
import { AdminCoursesComponent } from "./modules/admin/components/admin-courses/admin-courses.component";
import { LoginService } from "./modules/authen/services/login.service";
import { AdminService } from "./modules/admin/services/admin.service";
import { DescriptionComponent } from "./modules/courses/components/description/description.component";
import { MySpaceComponent } from "./modules/userSpace/components/my-space/my-space.component";
import { PanierComponent } from "./modules/shoppingCard/components/panier/panier.component";
import { SuccessOrderComponent } from "./modules/orders/components/success-order/success-order.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "courses",
    component: CoursesComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "about",
    component: AboutComponent
  },
  {
    path: "orders",
    component: OrdersComponent,
    canActivate: [LoginService]
  },
  {
    path: "admin-courses",
    component: AdminCoursesComponent,
    canActivate: [LoginService, AdminService]
  },
  {
    path: "description/:id",
    component: DescriptionComponent
  },
  {
    path: "mySpace",
    component: MySpaceComponent,
    canActivate: [LoginService]
  },
  {
    path: "cart",
    component: PanierComponent,
    canActivate: [LoginService]
  },
  {
    path: "success-orde/:id",
    component: SuccessOrderComponent,
    canActivate: [LoginService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
