import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { environment } from "../../src/environments/environment";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material-ui.module";
import { AppModuleAdmin } from "./modules/admin/app.module";
import { AppModuleAuth } from "./modules/authen/app.module";
import { AppModuleCommun } from "./modules/commun/app.module";
import { AppModuleCourses } from "./modules/courses/app.module";
import { AppModuleMenu } from "./modules/menu/app.module";
import { AppModuleOrders } from "./modules/orders/app.module";
import { AppModuleUsers } from "./modules/users/app.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AppModuleShoppingCarg } from "./modules/shoppingCard/app.module";
import { AppModuleUserSpace } from "./modules/userSpace/app.module";
import { AppModulePayment } from "./modules/payment/app.module";
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppModuleAdmin,
    AppModuleAuth,
    AppModuleCommun,
    AppModuleCourses,
    AppModuleMenu,
    AppModuleOrders,
    AppModuleUsers,
    AppModuleUserSpace,
    ReactiveFormsModule,
    FormsModule,
    AppModuleShoppingCarg,
    AppModulePayment
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
