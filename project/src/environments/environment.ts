// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBfr93BuOihpKb7uRRWyMueDSyh0mvDZiQ",
    authDomain: "vgparis-228fc.firebaseapp.com",
    databaseURL: "https://vgparis-228fc.firebaseio.com",
    projectId: "vgparis-228fc",
    storageBucket: "vgparis-228fc.appspot.com",
    messagingSenderId: "783148297685",
    appId: "1:783148297685:web:2ee259cc3b6eea1309a15c",
    measurementId: "G-SPCNNETF9P"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
