const express = require("express");
const bodyParser = require("body-parser");
const nodeMailer = require("nodemailer");
const app = express();
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post("/send-email", (req, res) => {
  console.log("api");
  // Grab the form data and send email
  let transporter = nodeMailer.createTransport({
    // host: "smtp.gmail.com",
    // port: 465,
    service: "Gmail",
    secure: false, // true for 465, false for other ports
    tls: {
      ciphers: "SSLv3"
    },
    secureConnection: false,
    auth: {
      // should be replaced with real sender's account
      user: "myapp.test121@gmail.com", // generated ethereal user
      pass: "mutgab11" // generated ethereal password
    },
    debug: false,
    logger: true
  });

  let mailOptions = {
    // should be replaced with real recipient's account
    from: "myapp.test121@gmail.com",
    to: "alyonamva18@gmail.com",
    // to: "zoolsudany6@gmail.com",
    // to: "aluq93@gmail.com",
    subject: "VG",
    html: `Demande d'informations:  <br> Name : ${req.body.name}  <br> Phone : ${req.body.phone}  <br> Message : ${req.body.message}   `
  };
  console.log(mailOptions);

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log("ok");
  });

  res.end();
});

app.listen(3000, () => {
  console.log("port 3000");
});
